# `python/qa`

Configures all jobs to perform quality assurance (Q/A) scans of your Python project.

## Description

This meta-component combines the following individual components

- [`python/code-quality`](./code-quality.md)
- [`python/dependency-scanning`](./dependency-scanning.md)
- [`sast/sast`](https://git.ligo.org/explore/catalog/computing/gitlab/components/sast)

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/qa@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `project_dir` | `"."` | Python project path (e.g. path containing the `pyproject.toml` file) |
| `stage` | `test` | Pipeline stage to add jobs to |
| `python` | `"python"` | Name of the Python interpreter to use for [`python/code-quality`](./code-quality.md) |
| `requirements` | None | Extra requirements to install with `pip` for [`python/code-quality`](./code-quality.md). |
| `code_quality_analyzer` | `"flake8"` | Code quality analyzer to configure |
| `code_quality_options` | `""` | Options to pass to the code quality analyzer |
| `run_advanced_sast` | `false` | Use [GitLab Advanced SAST](https://git.ligo.org/help/user/application_security/sast/gitlab_advanced_sast.html) |
| `sast_excluded_paths` | `"spec, test, tests, tmp"` {: .nowrap } | Paths to exclude from SAST (comma-separated string) |
| `sast_excluded_analyzers` { .nowrap } | `""` | Analyzers to exclude from SAST (comma-separated string) |

!!! info "`run_advanced_sast` will default to true in version 2"

    The `run_advanced_sast` input will be updated to default to `true`
    starting in version 2 of this component.

    To opt-in, or opt-out of the new default now, add the
    `run_advanced_sast` input option to your CI/CD configuration.

## Customisation

See the _Customisation_ sections for the relevant sub-components for
any other relevant details.

## Examples

!!! example "Configure complete quality-assurance scanning for a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/qa@<VERSION>
        inputs:
          code_quality_analyzer: ruff
    ```
