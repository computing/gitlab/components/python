# `python/code-quality`

Configure a job to scan a Python project for code quality issues.

## Description

This component just provides a convenient interface to one of the
analyzer-specific CI/CD components:

- [`python/flake8`](./flake8.md)
- [`python/ruff`](./ruff.md)

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/code-quality@<VERSION>
    inputs:
      analyzer: flake8
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `analyzer` | `"flake8"` | Code quality analyzer to configure |
| `project_dir` | `"."` | Python project path to scan |
| `python` | `"python"` | Name of the Python interpreter to call |
| `requirements` | None | Extra requirements to install with `pip`. |
| `stage` | `"test"` | Pipeline stage to add job to. |
| `analyzer_options` { .nowrap } | `""` | Extra options to pass to the analyzer |
