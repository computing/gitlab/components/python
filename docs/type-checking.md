# `python/type-checking`

Configure a job to run a static type checker against a Python project for
issues using
[mypy](https://mypy-lang.org/).

## Description

This component configures a single job called `type-checking` that installs `mypy`,
and then runs the tool.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/type-checking@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `fail_on_findings` { .nowrap } | `false` | If `false` (default) mark the job successful even if there are `mypy` 'error' findings |
| `mypy_options` | `""` | Options to pass to `mypy` |
| `project_dir` | `"."` | Python project path to scan |
| `python` | `"python"` | Name of the Python interpreter to call |
| `requirements` | None | Extra requirements to install with `pip`. |
| `stage` | `"test"` | Pipeline stage to add job to. |

## Customisation

All mypy customisation should be handled via the supported configuration
files; probably one of `pyproject.toml`, `setup.cfg` or `mypy.ini`.

For details, see <https://mypy.readthedocs.io/en/stable/config_file.html>.

## Examples

### Basic scan { #scan }

!!! example "Scan a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/type-checking@<VERSION>
    ```

### Scan a project in a subdirectory { #subdirectory }

If your Python project is in a subdirectory of the main gitlab project,
you should specify the `project_dir` input to tell `mypy` what to scan,
and also the `mypy_options` input to specify the configuration file that
`mypy` should use`.

!!! example "Scan a Python project in a subdirectory"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/type-checking@<VERSION>
        inputs:
          project_dir: myproject
          mypy_options: "--config-file myproject/pyproject.toml"
    ```
