# `python/dependency-scanning`

Configures a `dependency_scanning` job that uses that analyses the dependencies
of the project for known vulnerabilities.

## Description

This component builds on GitLab's upstream
[dependency scanning](https://git.ligo.org/help/user/application_security/dependency_scanning/index.html)
template to add support for projects that only provide `pyproject.toml` and no
other requirements or metadata files.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `project_dir` | `"."` | Path to scan for dependencies (e.g. path containing the `pyproject.toml` file) |
| `stage` | `"test"` { .nowrap } | Pipeline stage to add job to. |
| `before_script` { .nowrap } | `[]` | Extra commands to run before the scanner is executed |

## Examples

### Scan a Python application { #python }

!!! example "Scan a Python application"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/dependency-scanning@<VERSION>
    ```

### Install custom requirements before scanning { #before_script }

The Python dependency scanner works by installing a project's requirements
using `pip`.
In order to achieve this, it may be required to install some system packages
against which Python libraries can be linked dynamically at install time.
This can be simplified by specifying the system installation commands as part
of the `before_script` input:

!!! example "Specify custom `before_script`"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/dependency-scanning@<VERSION>
        inputs:
          before_script:
            - apt-get -yqq update
            - apt-get -yqq install libkrb5-dev
    ```

If using a meta-component that doesn't support specifying `before_script as
an input, you can specify the same commands directly by overwriting the
`dependency_scanning` template:

!!! example "Specify custom `before_script` in the job template"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>

    # overwrite before_script
    dependency_scanning:
      before_script:
        - apt-get -yqq update
        - apt-get -yqq install libkrb5-dev
    ```

!!! info "The dependency scanner runs in a Debian-based image"

    The dependency scanning tool runs inside a container image based on
    Debian, so system packages should be installed using `apt-get`.
