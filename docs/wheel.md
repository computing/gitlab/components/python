# `python/wheel`

Configure jobs to build binary wheels for the project.

## Description

This component creates one or more jobs that use
[`build`](https://build.pypa.io/) to create binary distributions (wheels)
for a Python project.

This component is able to configure matrices of jobs to support platform-
and version-specific wheels,
utilising [`manylinux`](https://github.com/pypa/manylinux) on Linux.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The pipeline stage to add jobs to. |
| `project_dir` | `"."` | Python project root directory. |
| `pure_python` | `true` | If `true` build a single wheel for all platforms and Python versions, otherwise configure a matrix of jobs to build for different versions. |
| `image` | See :arrow_right: | Container image in which to build wheels on Linux, default is [`python`](https://hub.docker.com/_/python) (if `pure_python: true`) or [`quay.io/pypa/manylinux_2_28_x86_64`](https://quay.io/repository/pypa/manylinux_2_28_x86_64) (`pure_python: false`). |
| `python_versions` | `["3.8", "3.9", "3.10", "3.11", "3.12", "3.13"]` {: .nowrap } | The list of Python versions to build when building with `pure_python: false`. |
| `job_name` | `"wheel"` | Name to give the job (or matrix of jobs). When building for multiple platforms, give each set a different value for `job_name`. |
| `runner_tags` | `[]` | List of runner [`tags`](https://git.ligo.org/help/ci/yaml/#tags) to apply to the job. |
| `build_options` | `""` | Extra options to pass to `python -m build`. |
| `delocate_options` {: .nowrap } | `""` | Extra options to pass to the wheel delocation tool (auditwheel on linux, delocate on macOS). |

## Customisation

The behaviour of the `build` tool should be customised via the
[`pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/)
project configuration file.

## Examples

### Building a wheel for a pure-Python project {: #build }

!!! example "Build a wheel for a pure-Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          pure_python: true
    ```

### Platform-specific wheels for Linux and macOS {: #compiled }

Projects that build Python extension modules, normally using the Python C
API, Cython, or SWIG, normally require a unique wheel for each platform
and for each minor version of Python.

This can be configured by including the `python/wheel` component once
for each platform, customising the `inputs` appropriately.

!!! example "Build platform-specific wheels"

    ```yaml
    include:
      # linux wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_linux_x86_64
          pure_python: false
          python_versions: &python_versions
            - 3.12
            - 3.13
      # macos wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_macos_x86_64
          pure_python: false
          python_versions: *python_versions
          runner_tags: [macos]
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_macos_arm64
          pure_python: false
          python_versions: *python_versions
          runner_tags: [macos_arm64]
    ```

### Platform-specific wheels with the Python limited API {: #limited }

Python presents a subset of the full C API via the
[Limited C API](https://docs.python.org/3/c-api/stable.html#limited-c-api)
which can be used to build extensions once that work with multiple versions
of Python.

To build wheels using this system, configure the `python/wheel` component using
a single version of Python that matches the `setuptools` `py_limited_api`
configuration parameter:

!!! example "Build platform-specific wheels with the Limited API"

    ```yaml
    include:
      # linux wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_linux_x86_64
          pure_python: false
          python_versions: &python_versions [3.9]
      # macos wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_macos_x86_64
          pure_python: false
          python_versions: *python_versions
          runner_tags: [macos]
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          job_name: wheel_macos_arm64
          pure_python: false
          python_versions: *python_versions
          runner_tags: [macos_arm64]
    ```
