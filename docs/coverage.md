# `python/coverage`

Configures a job to aggregate and report test coverage.

## Description

This component configures a job in the
[`.post`](https://git.ligo.org/help/ci/yaml/index.html#stages) stage
(by default) that combines coverage XML reports from individual `python/test`
jobs and reports a single
[code coverage result](https://git.ligo.org/help/ci/testing/code_coverage.html#code-coverage-results) for the pipeline.

This is required to work around a limitation in GitLab's default
[coverage](https://git.ligo.org/help/ci/yaml/index.html#coverage)
parser, which averages coverage _values_ for multiple jobs.
This down-ranks coverage from projects that may have lower coverage on some
platforms (e.g. Windows).

To get around this limitation, the `python/coverage` component uses
`coverage.py`'s
[`combine`](https://coverage.readthedocs.io/en/latest/cmd.html#cmd-combine)
command to correctly merge coverage XML reports from multiple jobs, enabling
reporting of a correct single statistic for the whole pipeline.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/coverage@<VERSION>
```

!!! info "This component is automatically included by `python/test`"

    This component is automatically included by the [`python/test`](test.md)
    component, so likely doesn't need to be manually configured.

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `python_coverage` { .nowrap } | The name to give the job. |
| `stage` | `.post` | Pipeline stage to add jobs to |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image to use to run this job |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `coverage_rcfile` { .nowrap } | None | Path to coverage.py configuration file (helps with providing accurate coverage measurements, autodiscovered in most cases) |

## Customisation

Coverage reporting should be customised via the
[`[tool.coverage]`](https://coverage.readthedocs.io/en/latest/config.html#toml-syntax)
table of the project `pyproject.toml` configuration file.

!!! example "Configure coverage.py in `pyproject.toml`"

    ```toml
    [tool.coverage.paths]
    # map paths from installed locations back to project source
    source = [
      "myproject/",  # <-- source path
      "*/myproject/",  # <-- any installed path
    ]

    [tool.coverage.report]
    precision = 1
    # omit auto-generated version file
    omit = [
      "*/_version.py",
    ]
    ```
