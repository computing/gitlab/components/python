# Python CI/CD components

This `computing/gitlab/components/python` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a [Python](https://www.python.org) project.

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/python/-/badges/release.svg?text=test)](https://git.ligo.org/explore/catalog/computing/gitlab/components/python/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`python/code-quality`](./code-quality.md "`python/code-quality` component documentation")
- [`python/coverage`](./coverage.md "`python/coverage` component documentation")
- [`python/dependency-scanning`](./dependency-scanning.md "`python/dependency-scanning` component documentation")
- [`python/publish`](./publish.md "`python/publish` component documentation")
- [`python/sdist`](./sdist.md "`python/sdist` component documentation")
- [`python/test`](./test.md "`python/test` component documentation")
- [`python/type-checking`](./type-checking.md "`python/type-checking` component documentation")
- [`python/wheel`](./wheel.md "`python/wheel` component documentation")

In addition the following meta-components (combinations) of the above components
are available:

- [`python/all`](./all.md "`python/all` component documentation")
- [`python/qa`](./qa.md "`python/qa` component documentation")
