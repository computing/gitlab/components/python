# `python/publish`

Configures a job to publish a Python project to [PyPI](https://pypi.org).

## Description

This component discovers [source](./sdist.md) and [binary](./wheel.md)
distributions (`*.tar.*` and `*.whl` files) and uploads them to
[PyPI](https://pypi.org) using [twine](https://twine.readthedocs.io).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/publish@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `"deploy"` | The pipeline stage to add jobs to |
| `job_name` | `"twine"` | Name to give the publishing job |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `files` | `"*.tar.* *.whl"` {: .nowrap } | Filename or glob pattern(s) for files to publish" |
| `twine_options` {: .nowrap } | `"--verbose"` | Options to pass to `twine upload` |

## Notes

### Credentials

The `python/publish` component uses [twine](https://twine.readthedocs.io) to
interact with PyPI, and requires a token for authentication.
For details on creating a token, see <https://pypi.org/help/#apitoken>.

The token should be stored as a
[group or project variable](https://git.ligo.org/help/ci/variables/index.md#add-a-cicd-variable-to-a-project)
with the name `TWINE_PASSWORD`, and should ***not*** be stored in the
project `.gitlab-ci.yml` file.

This component is not configured for PyPI's 'trusted publishing'.
Support for this may be added in the future.

## Examples

### Publishing specific distributions {: #files }

By default, the `python/publish` component will attempt to publish all tarballs
(matching `*.tar.*`) and all Python wheels (matching `*.whl`) from *all*
previous-stage jobs in the pipeline.

To ensure that only the correct files are published, it is recommended
that you specify the `files` input with the appropriate patterns, e.g:

!!! example "Publishing files matching a specific pattern"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/publish@<VERSION>
        inputs:
          files: "myproject-{*.tar.*,*.whl}"
    ```

### Publishing to the GitLab project package register {: #gitlab }

The `python/publish` component works with the GitLab project
[package registry](https://git.ligo.org/help/user/packages/pypi_repository/index.html).
To publish a package to that registry, use the following value for the
`twine_options` input:

!!! example "Publish to the gitlab package registry"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/publish@<VERSION>
        inputs:
          twine_options: "-u gitlab-ci-token -p $CI_JOB_TOKEN --repository-url $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi"
    ```
