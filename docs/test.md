# `python/test`

Configures jobs to test a Python project.

## Description

This component installs the target Python project and then runs
[`pytest`](https://docs.pytest.org/) to execute the project
test suite.

In the simplest case (with no `inputs` given), this component configures
a job matrix that, for multiple versions of Python, executes _roughly_ the
following commands:

```shell
python -m pip install pytest pytest-cov
python -m pip install .
python -m pytest --cov . --junit-xml junit.xml .
```

However, the various `inputs`, and the layout of the project,
dynamically influence the script.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `python_test` { .nowrap } | The name to give the job. |
| `stage` | `test` | Pipeline stage to add jobs to |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `conda` | `false` | If `true`, use `conda` to populate the test environment. See [_Conda_](#conda) below for more details |
| `needs` | `[]` | List of jobs whose artifacts are needed by the test jobs |
| `install_target` | `"."` | Path/file/package to install (supports wildcards) |
| `install_extra` | None | Name of the [extra feature group(s)](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/#dependencies-and-requirements) to install (comma-separated) |
| `pytest_options` | `""` | Extra options to pass to pytest |
| `extra_test_commands` {: .nowrap } | `[]` | Extra test commands to run (after pytest) |
| `coverage` | `true` | If `true`, include [`python/coverage`](./coverage.md) to automatically produce a code coverage result |
| `coverage_rcfile` {: .nowrap } | None | Path to coverage.py configuration file (helps with providing accurate coverage measurements, autodiscovered in most cases) |

## Notes

### Conda

If `conda: true` is given in the `inputs` to the `python/test` component,
the following changes are made to the configured job matrix:

-   The job matrix is called `{ job_name }_conda`, e.g. `python_test_conda`.

-   A custom `before_script` is configured that uses
    [`pip2conda`](https://github.com/duncanmmacleod/pip2conda/) to create
    a conda environment called `test` in which the tests will run.

-   The `install_extra` input can accept a new value `"--all"` which
    triggers `pip2conda` to install packages for all optional dependencies
    keys in the `pyproject.toml`/`setup.cfg`/`setup.py` files.

-   Jobs will cache downloaded conda packages as well as pip downloads.

The execution of `pytest` and the details of the coverage reporting are
unchanged.

### Automatic test and coverage reporting {: #reports }

The tests are executed using [pytest](https://pytest.org/) and automatically
include coverage reporting using
[pytest-cov](https://pypi.org/project/pytest-cov/) and
[Unit test reports](https://git.ligo.org/help/ci/testing/unit_test_reports.html).

## Customisation

### pytest

`pytest`'s behaviour can be be customised in one of the following ways
(ordered by preference, most recommended to least):

-   by specifying the
    [`tool.pytest.ini_options`](https://docs.pytest.org/en/stable/reference/customize.html#pyproject-toml)
    table in the project `pyproject.toml` configuration file.

    !!! example "Configure pytest in `pyproject.toml`"

        ```toml
        [tool.pytest.ini_options]
        addopts = "-ra --cov myproject"
        ```

-   give the `pytest_options` [`input`](#inputs)

    !!! example "Configure pytest through the component `inputs`"

        ```yaml
        include:
          - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>
            inputs:
              pytest_options: "-ra -v"
        ```

-   set the `PYTEST_ADDOPTS`
    [variable](https://git.ligo.org/help/ci/variables/#define-a-cicd-variable-in-the-gitlab-ciyml-file)
    for the `python_test` job:

    !!! example "Set `PYTEST_ADDOPTS` for the `python_test` job"

        ```yaml
        include:
          - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>

        python_test:
          variables:
            PYTEST_ADDOPTS: "-k 'not bad_test'
        ```

### coverage.py

Coverage gathering should be customised via the
[`[tool.coverage]`](https://coverage.readthedocs.io/en/latest/config.html#toml-syntax)
section of the project `pyproject.toml` configuration file.

!!! example "Configure coverage.py in `pyproject.toml`"

    ```toml
    [tool.coverage.paths]
    # map paths from installed locations back to project source
    source = [
      "myproject/",  # <-- source path
      "*/myproject/",  # <-- any installed path
    ]

    [tool.coverage.report]
    precision = 1
    # omit auto-generated version file
    omit = [
      "*/_version.py",
    ]
    ```

## Examples

### Testing a Python project {: #test }

!!! example "Run tests for a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>
        inputs:
          install_extra: "test"
          pytest_options: "-ra -v"
          python_versions:
            - "3.12"
            - "3.13"
    ```

### Executing tests from an installed library {: #pyargs }

To run tests for an installed library, rather than from the project
directory, specify `install_target` to install the distribution and
`pytest_options` to target it using the `--pyargs` option:

!!! example "Running tests for an installed library"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          project_dir: example/
      - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>
        inputs:
          job_name: "pytest"
          install_target: "*.whl"
          install_extra: "test"
          needs: [wheel]
          pytest_options: "-ra --pyargs example_project"
          extra_test_commands:
            - my_cli --help
          python_versions:
            - "3.12"
            - "3.13"
    ```
