# `python/ruff`

Configure a job to scan a Python project for issues using
[Ruff](https://docs.astral.sh/ruff/).

## Description

This component configures a single job called `ruff`
that installs [`ruff`](https://pypi.org/project/ruff/),
and then runs the tool to produce a GitLab
[Code Quality report](https://git.ligo.org/help/ci/testing/code_quality.html).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/ruff@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `ruff_options` { .nowrap } | `""` | Extra options to pass to `ruff` |
| `project_dir` | `"."` | Python project path to scan |
| `python` | `"python"` { .nowrap } | Name of the Python interpreter to call |
| `requirements` | None | Extra requirements to install with `pip`. |
| `stage` | `"test"` | Pipeline stage to add job to. |

## Customisation

All ruff customisation should be handled via the supported configuration
files; probably one of `pyproject.toml` or `ruff.toml`.

For details, see <https://docs.astral.sh/ruff/configuration/>.

## Examples

### Scan a Python project { #scan }

!!! example "Scan a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/ruff@<VERSION>
    ```

### Configure Ruff via the `python/all` component { #all }

!!! example "Configure Ruff via `python/all`"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>
        inputs:
          code_quality_analyzer: ruff
    ```

### Install extra requirements { #requirments }

To install extra requirements, use the `requirements` input, passing arguments
as accepted by `pip`:

!!! example "Scan a project with extra requirements"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/ruff@<VERSION>
        inputs:
          requirements: "-r requirements.txt"
    ```
