"""Example Python project"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org"

from .get import get_json
try:
    from ._version import __version__  # type: ignore
except ModuleNotFoundError:  # dev mode
    __version__ = "dev"
