from typing import Any

import requests


def get_json(url: str, **kwargs) -> dict[str, Any]:
    """Get JSON from a URL.
    """
    resp = requests.get(url, **kwargs)
    resp.raise_for_status()
    return resp.json()
